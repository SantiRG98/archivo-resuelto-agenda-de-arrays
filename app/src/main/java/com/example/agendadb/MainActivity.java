package com.example.agendadb;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import database.AgendaContactos;
import database.Contacto;

public class MainActivity extends AppCompatActivity {

    private Button btnAgregar;
    private TextView lblNombre;
    private AgendaContactos db;
    ArrayList<Contacto> contactos = new ArrayList<>();
    private EditText edtNombre;
    private EditText edtTelefono;
    private EditText edtTelefono2;
    private EditText edtDireccion;
    private EditText edtNotas;
    private CheckBox cbxFavorito;
    private Contacto saveContact;
    private int savedIndex;
    private long indexActual;
    private Contacto savedContact;
    private long id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        db = new AgendaContactos(MainActivity.this);

        lblNombre = (TextView) findViewById(R.id.lblNombre);
        edtNombre = (EditText) findViewById(R.id.edtNombre);
        edtTelefono = (EditText) findViewById(R.id.edtTel1);
        edtTelefono2 = (EditText) findViewById(R.id.edtTel2);
        edtDireccion = (EditText) findViewById(R.id.edtDomicilio);
        edtNotas = (EditText) findViewById(R.id.edtNota);
        cbxFavorito = (CheckBox) findViewById(R.id.chkFavorito);

        btnAgregar = (Button) findViewById(R.id.btnGuardar);
        Button btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        Button btnListar = (Button) findViewById(R.id.btnListar);
        Button btnCerrar = (Button) findViewById(R.id.btnCerrar);



        btnAgregar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(edtNombre.getText().toString().matches("") || edtTelefono.getText().toString().matches("") ||
                        edtTelefono2.getText().toString().matches("") || edtDireccion.getText().toString().matches("")) {
                    Toast.makeText(MainActivity.this, "Ingrese todos los datos", Toast.LENGTH_SHORT).show();
                } else {
                    Contacto nContacto = new Contacto();

                    nContacto.setNombre(edtNombre.getText().toString());
                    nContacto.setDomicilio(edtDireccion.getText().toString());
                    nContacto.setNotas(edtNotas.getText().toString());
                    nContacto.setTelefono1(edtTelefono.getText().toString());
                    nContacto.setTelefono2(edtTelefono2.getText().toString());
                    nContacto.setFavorito(cbxFavorito.isChecked() ? 1:0);
                    if (cbxFavorito.isChecked()){
                        nContacto.setFavorito(1);
                    }
                    else {
                        nContacto.setFavorito(0);
                    }
                    db.openDataBase();

                    if (saveContact == null){
                        long idx = db.insertarContacto(nContacto);
                        Toast.makeText(MainActivity.this,"Se agregó el Registro con ID: " + idx, Toast.LENGTH_SHORT).show();
                    }
                    else{
                        db.UpdateContacto(nContacto,id);
                        Toast.makeText(MainActivity.this,"se Actualizó el Registro: " + id, Toast.LENGTH_SHORT).show();
                    }
                    db.cerrarDataBase();
                }
            }
        });


        btnListar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, ListActivity.class);
                Bundle bObject = new Bundle();
                db.openDataBase();
                contactos = db.allContactos();
                db.cerrarDataBase();
                bObject.putSerializable("contactos", contactos);
                bObject.putLong("indexActual", indexActual);
                i.putExtras(bObject);
                startActivityForResult(i, 0);
            }
        });
        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                limpiar();
            }
        });
        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        if (Activity.RESULT_OK == resultCode) {
            Contacto contacto = (Contacto) intent.getSerializableExtra("contacto");
            savedContact = contacto;
            id = contacto.get_ID();
            edtNombre.setText(contacto.getNombre());
            edtTelefono.setText(contacto.getTelefono1());
            edtTelefono2.setText(contacto.getTelefono2());
            edtDireccion.setText(contacto.getDomicilio());
            edtNotas.setText(contacto.getNotas());
            if (contacto.getFavorito() > 0) {
                cbxFavorito.setChecked(true);
            }
        } else {
            limpiar();
        }


    }

    public void limpiar() {
        edtNombre.setText("");
        edtDireccion.setText("");
        edtTelefono.setText("");
        edtTelefono2.setText("");
        edtNotas.setText("");
        cbxFavorito.setChecked(false);
        saveContact = null;
    }
}
